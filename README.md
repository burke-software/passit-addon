# Passit Addon


A WebExtension for the Passit system.

Will allow:

- [ ] Logging in
- [ ] Viewing all secrets in a popup
- [ ] Editing those secrets inline


## Developing

To compile Javscript for popup, run `npm run build`. Then you should be able to
open the extension in:

* [Firefox](https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Your_first_WebExtension#Testing_it_out)
* [Chrome](https://developer.chrome.com/extensions/getstarted#unpacked)


### Testing

We use JS standard to maintain some code quality and uniformity. Run `npm test`
to make sure all the code passes the litner.
