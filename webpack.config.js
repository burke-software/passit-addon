var HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  context: __dirname,
  entry: './src/main.js',
  output: {
    path: './dist',
    filename: 'bundle.js',
    pathinfo: true
  },
  plugins: [new HtmlWebpackPlugin({
    title: 'My app',
    template: 'src/index.html'
  })],
  module: {
    preLoaders: [
      {
        test: /\.(js|jsx)$/,
        loader: 'source-map-loader'
      }
    ],
    loaders: [
      {
        test: /\.(eot|woff|woff2|ttf|svg|png|jpg)$/,
        loader: 'url?limit=1000000'
      },
      { test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader' },
      { test: /\.css$/, loader: 'style-loader!css-loader?modules' }
    ]
  },
  devtool: 'source-map',
  debug: true
}
