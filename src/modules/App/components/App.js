import { Component } from 'cerebral-view-snabbdom'
import '!style!css!bootstrap/dist/css/bootstrap.css'

import styles from './App.css'
import Auth from '../modules/Auth/component'

export default Component(({state}) => (
  <div classNames={['container', styles.app]}>
    <Auth />
  </div>
))
