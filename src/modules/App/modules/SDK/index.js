function fakeAuth (user) {
  return new Promise((resolve, reject) => {
    window.setTimeout(
      function () {
        if (user.password === 'saul') {
          resolve({token: 'hi', id: 1})
        } else {
          reject('Bad!')
        }
      },
      Math.random() * 2000 + 1000
    )
  })
}

export default (module) => {
  module.addServices({
    login: fakeAuth,
    signup: fakeAuth
  })
}
