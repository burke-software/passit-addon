export function login ({input, output, services, modules}) {
  services.app.sdk.login(input.user)
    .then((token) => output.success({token}))
    .catch((error) => output.error({error}))
}
login.async = true

export function signup ({input, output, services}) {
  services.app.sdk.signup(input.user)
    .then((token) => output.success({token}))
    .catch((error) => output.error({error}))
}
signup.async = true
