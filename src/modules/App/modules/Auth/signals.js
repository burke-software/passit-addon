import set from 'cerebral-addons/set'
import copy from 'cerebral-addons/copy'

import {login, signup} from './../SDK/actions'

function authActions (asyncAction) {
  return [
    set('state:/app.auth.isSubmitting', true),
    copy('state:/app.auth.form', 'output:/user'),
    asyncAction, {
      success: [
        copy('input:/token', 'state:/app.auth.user.token'),
        copy('state:/app.auth.form.email', 'state:/app.auth.user.email'),
        set('state:/app.auth.form.email', ''),
        set('state:/app.auth.form.password', ''),
        set('state:/app.auth.error', '')
      ],
      error: [
        copy('input:/error', 'state:/app.auth.error')
      ]
    },
    set('state:/app.auth.isSubmitting', false)
  ]
}

export default {
  clickedLogIn: authActions(login),
  clickedSignUp: authActions(signup),
  emailChanged: {chain: [copy('input:/email', 'state:/app.auth.form.email')], immediate: true},
  passwordChanged: {chain: [copy('input:/password', 'state:/app.auth.form.password')], immediate: true}
}
