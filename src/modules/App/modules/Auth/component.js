import { Component } from 'cerebral-view-snabbdom'

let Email = Component({
  email: ['app', 'auth', 'form', 'email']
}, ({signals, state}) => (
  <input
    type='email'
    className='form-control'
    placeholder='Email'
    value={state.email}
    on-change={(e) => signals.app.auth.emailChanged({email: e.target.value})}
  />
))

let Password = Component({
  password: ['app', 'auth', 'form', 'password']
}, ({signals, state}) => (
  <input
    type='password'
    className='form-control'
    placeholder='Password'
    value={state.password}
    on-change={(e) => signals.app.auth.passwordChanged({password: e.target.value})}
  />
))

let LogIn = Component(({signals}) =>
  <button
    type='button'
    className='btn btn-default'
    on-click={() => signals.app.auth.clickedLogIn({})}
  >Log In</button>
)

let SignUp = Component(({signals}) =>
  <button
    type='button'
    className='btn btn-default'
    on-click={() => signals.app.auth.clickedSignUp({})}
  >SignUp</button>
)

let Error = Component({
  error: ['app', 'auth', 'error']
}, ({state}) => (
  state.error ? (<p className='text-danger'>{state.error}</p>) : (<div></div>))
)

export default Component(() => (
  <div className='row'>
    <div className='col-xs-12'>
      <h1>Passit</h1>
      <form>
        <Email />
        <Password />
        <Error />
        <div className='btn-group' props-role='group'>
          <LogIn />
          <SignUp />
        </div>
      </form>
    </div>
  </div>
))
