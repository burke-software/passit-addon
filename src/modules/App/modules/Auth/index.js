import signals from './signals'

export default (module) => {
  module.addState({
    form: {
      email: '',
      password: ''
    },
    user: {
      token: null,
      id: null
    },
    error: null,
    isSubmitting: false
  })

  module.addSignals(signals)
}
