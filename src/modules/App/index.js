import Auth from './modules/Auth'
import SDK from './modules/SDK'

export default (module) =>
  module.addModules({
    auth: Auth,
    sdk: SDK
  })

