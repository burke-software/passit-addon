import Controller from 'cerebral'
import { Component, render } from 'cerebral-view-snabbdom' // eslint-disable-line no-unused-vars
import Model from 'cerebral-model-immutable-js'
import Devtools from 'cerebral-module-devtools'

import App from './modules/App/components/App'
import AppModule from './modules/App'

const controller = Controller(Model({}))

controller.addModules({
  devtools: Devtools(),

  app: AppModule
})

render(() => <App/>, document.querySelector('#app'), controller)
